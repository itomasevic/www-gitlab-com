module Gitlab
  module Homepage
    class Team
      class Member
        include Comparable
        attr_reader :assignments

        def self.normalize_country(country)
          case country
          when 'USA'
            'United States'
          when 'The Netherlands'
            'Netherlands'
          else
            country
          end
        end

        def <=>(other)
          sort_ordering = (start_date.nil? ? Date.today : start_date) <=> (other.start_date.nil? ? Date.today : other.start_date)

          return 0 if sort_ordering.nil?

          sort_ordering
        end

        def ==(other)
          slug == other.slug
        end
        alias_method :eql?, :==

        def hash
          [self.class, slug].hash
        end

        def initialize(data)
          @data = data
          @assignments = []
        end

        def anchor
          twitter || gitlab || name.parameterize
        end

        def username
          @data.fetch('gitlab')
        end

        def involved?(project)
          project_roles.has_key?(project.key)
        end

        def text_role
          @data.fetch('role', '').gsub(/<[^>]+>/, '')
        end

        def project_roles
          @project_roles ||= {}.tap do |hash|
            @data.fetch('projects', {}).each do |project, roles|
              Array(roles).each { |role| (hash[project] ||= []) << role }
            end
          end
        end

        def assign(project)
          project_roles[project.key].each do |role|
            @assignments << Team::Assignment.new(self, project, role)
          end
        end

        def departments
          @departments ||= @data.fetch('departments', [])
        end

        def country_normalized
          self.class.normalize_country(@data['country'])
        end

        def country_info
          @country_info ||= ISO3166::Country.find_country_by_name(country_normalized)
        end

        ##
        # Middeman Data File objects compatibiltiy
        #
        def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissingSuper
          @data[name.to_s]
        end

        def respond_to_missing?(method_name, include_private = false)
          @data.include?(method_name.to_s) || super
        end

        def self.all!
          @members ||= YAML.load_file('data/team.yml')
          @titles ||= {}.tap do |hash|
            @members.each do |member|
              hash[member['slug']] = member['role'] if member['slug']
            end
          end

          @members.map do |data|
            reports_to = data['reports_to']
            data['reports_to_title'] = @titles[reports_to] if reports_to
            new(data).tap { |member| yield member if block_given? }
          end
        end

        def self.no_vacancies
          @no_vacancies ||= all!.reject { |member| member.name == 'New Vacancy' }
        end
      end
    end
  end
end
