---
layout: job_family_page
title: "UX Designer"
---

At GitLab, UX Designers collaborate closely with Product Managers, Engineers, UX Research, and other UX Designers to create an [productive, minimal, and human](https://design.gitlab.com/) experience. UX Designers report to the UX Manager.

## Responsibilities

* Help to define and improve the interface and experience of GitLab.
* Design features that fit within the larger experience and flows.
* Create deliverables (wireframes, mockups, prototypes, flows, etc.) to communicate ideas.
* Work with Product Managers and Engineers to iterate on and refine the existing experience.
* Involve UX Researchers when needed and help them define research initiatives (usability tests, surveys, interviews, etc.).
* Stay informed and share the latest on UI/UX techniques, tools, and patterns.
* Understanding of responsive design and best practices.
* Have working knowledge of HTML, CSS. Familiarity with Javascript.
* Knowledge and understanding of design systems theory and practice.
* General knowledge of Git flow (feature branching, etc.), merge/pull requests, pipelines, and code testing.

#### UX Designer

* Break down designs to fit within the monthly release cadence and productively iterate across milestones.
* Actively evaluate and incorporate feedback from UX Research, share relevant user research findings and recommendations with team.
* Participate in the development and upkeep of the GitLab Design System.
* Participate in and help drive [product discovery](https://about.gitlab.com/handbook/product/#product-discovery-issues) to generate ideas for features and improvements.
* Take part in the monthly release process by reviewing and approving UX implementations.

#### Senior UX Designer

* Have knowledge of GitLab's UX history and direction.
* Serve as the go-to person with UX decisions.
* Propose, implement, and lead user experience improvements which impact multiple areas of the product.
* Mentor other members of the UX department.
* Entrust work to other members of the UX department as appropriate.
* Improve the UX department's productivity through the suggesting and vetting of tools and workflow improvements.
* Collaborate with team members across the organization to advocate for users and user-centered
design practices.
* Write blog posts about UX at GitLab.
* Interview potential UX candidates.

#### Staff UX Designer

* Assist the department in viewing the design of GitLab from a holistic and comprehensive perspective.
* Lead strategic UX decisions, balancing both short-term considerations and long-term vision.
* Propose and lead the implementation of major, cross-feature, broad projects when appropriate. This may be within a [stage group](/company/team/structure/#stage-groups/) or across multiple stage groups.
* Help to set direction and focus for GitLab's UX and vision.
* Proactively improve process and workflow by anticipating and addressing future needs for the UX department and the product.
* Bring clarity and understanding to abstract and ambiguous UX problem spaces.
* Continually connect and drive current work towards our [Product Vision](/direction/product-vision/).

## Tools

Tools used by the UX department are flexible depending on the needs of the work. Please see the [UX Designer Onboarding](/handbook/engineering/ux/uxdesigner-onboarding/) page for more details.

## Success Criteria

You know you are doing a good job as a UX Designer when:

* You collaborate effectively with Engineers, Product Managers, UX Designers, and UX researchers.
* You are resolving all UX / UI issues assigned to you in a milestone.
* You are contributing ideas and solutions beyond existing issues.
* Users are overwhelmingly happy about your contributions.


## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [UX Team](/handbook/engineering/ux/)
- [GitLab Design Kit](https://gitlab.com/gitlab-org/gitlab-design)
- [GitLab Design System](https://design.gitlab.com/)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute first interview with a UX Designer.
* Candidates will then be invited to schedule a 1 hour interview with the UX Manager. This interview will include a run-through of basic UX and UI dev skills as described in the responsibilities.
* Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering.
* Finally, candidates may be asked to schedule a 50 minute interview with our CEO.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
