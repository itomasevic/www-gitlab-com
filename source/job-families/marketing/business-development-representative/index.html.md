---
layout: job_family_page
title: "Business Development Representative"
---

You love talking about GitLab to people and no question or challenge is too big or small. You have experience working directly with prospects in order to answer questions on getting started with a technical product. Your job is to make sure our prospects are successful, from the single-shop development firm all the way up to our Fortune 500 customers, and that everyone gets the appropriate level of support.

## Responsibilities

* Be able to identify where a prospect is in the sales and marketing funnel and take the appropriate action.
* Develop and work with our business development manager to follow and improve the [processes](/handbook/marketing/marketing-sales-development/sdr/) for passing leads to sales, support, etc.
* Participate in documenting all processes in the handbook and update as needed with our business development team lead.
* Be accountable for your lead data and prospecting activities, log everything.
* Speed to lead; respond quickly to (or re-route) all inbound iqnuiries (email, live chat, etc.).
* Manage and help qualify and/or route inbound requests and inquiries.
* Discretion to qualify and disqualify leads when appropriate.
* Collaborate with our marketing team on messaging for demand generation campaigns.
* Work to introduce more Core users to our subscription offerings.
* Work closely with sales to identify new pain points to gain a better understanding of the benefits of Ultimate.
* Strategize with our business development manager to develop the proper qualifying questions for all types of customers.


## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service
* Experience with CRM and/or marketing automation software
* An understanding of B2B software, Open Source software, and the developer product space is preferred
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Passionate about technology and learning more about GitLab
* Be ready to learn how to use GitLab and Git
* Start part-time or full-time depending on situation
* You share our [values](/handbook/values), and work in accordance with those values.
* If in EMEA, fluency in spoken and written German and/or other European languages is preferred.
* If in APAC, fluency in Mandarin or literacy in Simplified Chinese is required.
* If in LATAM, fluency in Portuguese and Spanish is required.
*
## Senior Business Development Representative

As a Senior Business Development Representative (BDR) You love talking about GitLab to people! No question or challenge is too big or small. You have experience working directly with customers in order to answer questions on getting started with a technical product. In addition, it is expected that you will lead the BDR team from the front in every way that the team is measured.

### Responsibilities

* Meet or exceed Senior Business Development Representative (BDR) sourced Sales Qualified Pipeline targets.
* Act as a mentor for new BDR hires in helping them navigate their Marketing Qualified Leads (MQLs).
* Participate in documenting all processes in the handbook and update as needed with our Sales and Business Development Manager.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from one of our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Business Development Manager
* Candidates will then be invited to schedule an interview with the Director of Sales Development
* Candidates will be invited to schedule a third interview with our CMO
* Finally, candidates may be asked to interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring)
