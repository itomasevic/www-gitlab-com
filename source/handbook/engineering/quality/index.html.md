---
layout: markdown_page
title: "Quality"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Child Pages
* [On-boarding](/handbook/engineering/quality/onboarding)
* [Project Management](/handbook/engineering/quality/project-management)
* [Test Engineering](/handbook/engineering/quality/test-engineering)
* [Issue Triage](/handbook/quality/issue-triage/)


## Vision

Ensure that GitLab consistently releases high-quality software across the growing suite of products, developing software at scale without sacrificing quality, stability or velocity.
The quality of the product is our collective responsibility. The quality department makes sure everyone is aware of what the quality of the product is, empirically.

To execute on this, we categorize our vision into the following areas.

### Culture of Quality

Ensure a culture of quality early on in the development process.
  * Review product requirements and designs to identify risk and steer the team on quality strategy.
  * Partner with all engineering teams to ensure that our products have testability built in that can be leveraged by the test infrastructure.
  * Be a champion for better software design, promote proper testing practices and bug prevention strategies.

Create a timely feedback loop on Quality.    
  * Use the data from test results to improve test gaps in our test suites.
  * Conduct test gap analysis from bugs that are filed by GitLab users.
  * Be a sounding-board for our end users and non-engineering stakeholders by integrating their feedback on quality into the development process.

### Test Automation Expertise

Own and build out test automation frameworks and infrastructure.
  * Own and expand end-to-end automated testing framework [GitLab QA].
  * Own and build functional performance tests.
  * Own and build third-party service testing capabilities.
  * Own and build our Continuous Integration & Delivery test pipelines.

Continuously improve GitLab test coverage, reliability and efficiency.
  * Groom the [test pyramid](https://martinfowler.com/bliki/TestPyramid.html) coverage and ensure that the right tests run at the right place.
  * Improve on the duration and de-duplication of the GitLab test suites.
  * Improve stability by eliminating flaky tests and transient failures.
  * Improve test results tracking and reporting mechanisms that aid in triaging test results.
  * Coach developers (internal & external) on contributing to [GitLab QA] test scenarios.

Continuously improve the release process where Quality is a stakeholder.
  * Eradicate obstacles in the release process.
  * Ensure that we have a repeatable and consistent Quality process for every release.

### Developer productivity

* Make metrics-driven suggestions to improve engineering processes, velocity, and throughput.
* Build productivity tooling to help speed up development efforts.
* Build automated tools to ensure the consistency and quality of the codebase and merge request workflow.

See more details on the [Developer productivity team page](developer-productivity-team).

Make metrics-driven suggestions to improve engineering processes, velocity, and throughput.
  * Build automated [measurements and dashboards](http://quality-dashboard.gitlap.com/groups/gitlab-org) to gain insights into developer productivity and understand what is working and what is not.
    * See [GitLab Insights] project.
    * Make suggestions for improvements, monitor the results and iterate.
  * Ensure workflow and label hygiene in our system which feeds into our metrics dashboard.
    * See [GitLab Triage] project.

Build productivity tooling to help speed up development efforts
  * Automated Merge Request assistant.
  * Increase contributor and developer productivity by improving the development setup, workflow, processes, and tools.
    * Improve the ease of use of our GitLab Development Kit.
    * Improve Review Apps for CE/EE (GDK in the cloud).

## Teams

### [Developer productivity team](developer-productivity-team)

### [Dev stage cross-functional team](dev-stage-team)

### [Ops stage cross-functional team](ops-stage-team)

### Future teams

**Experience and Performance team**

The need to ensure performance and quality will not diminish as we scale, so we
may consider adding a dedicated team in the future.

Currently this is being handled by each test area owners.

* Functional Performance tests
  * An emphasis on the performance of on-premise GitLab installations
* Browser and mobile browser coverage
  * Functionality testing against all supported customer browsers
  * Visual regression testing with real browsers

## Workload planning and assignment

Please look at our [Project Management](/handbook/engineering/quality/project-management) page 
for an overview of the projects we currently own and contribute to.

### Guidelines

* Product alignment teams ([Dev stage](dev-stage-team) & [Ops stage](ops-stage-team)) are stable-counterparts that are embedded as part of each product area's functional group. 
The scheduling is guided by each product area's product manager.
* The [Developer productivity team](developer-productivity-team) which is focused on improving internal processes schedules work independently but is driven by the productivity severity of the problem we are solving. 

### Submitting and reviewing code

For test automation changes, it is crucial that every change is reviewed by at least one Senior Test Automation Engineer in the Quality team. 

We are currently setting best practices and standards for Page Objects and REST API clients. Thus the first priority is to have test automation related changes reviewed and approved by the team. 
For test automation only changes, the quality team alone is adequate to review and merge the changes.  

It is strongly recommended not to re-assign or ask someone else to take over a Merge Request if the original engineer is not able to complete it immediately (e.g., due to vacation, illness, etc). 
An exception might be if the Merge Request can be completed quickly and easily (e.g., it requires only minor changes that someone else can easily make).

## Team retrospective

The Quality team holds a once-a-month 30 minute meeting for the team's retrospective. This happens one week before the public GitLab team retrospective.
The retrospective is for us to reflect as a team; what went well, what went wrong and how can we improve. Emotions are not only allowed in retrospectives, they are encouraged, this is a safe environment that allows the team to discuss freely. The meeting is private only to the Quality team, however, the agenda items are public within GitLab.
The agenda items are populated ahead of time, come meeting time, we discuss the items and identify a positive plan of action as needed.
Important findings here can make its way into the public GitLab team retrospective in the following week.

It should be made clear that this is a retrospective and we encourage frank and sincere self-reflection. As a result, we allow passionate and emotional comments in the agenda but we ensure that they are steered towards a positive plan of action.

Examples:
> * `Person A`: I was really disappointed by the progress of `x` because `y`. => `Person B`: I noticed that too, in the future let's make sure that we prevent `y` from happening with this plan etc.
> * `Person C`: I was really angry that `this` happened, we are not being efficient here => `Person D`: should we change the way we currently do things, what improvements can be made.

The Quality team [retrospective agenda](https://docs.google.com/document/d/1omIImDFKfW2AgYH-Ng8IK2wIAZoyZBe_6QyYt02hgIM/edit#) is only available for viewing inside GitLab.
The public team retrospective contains information that is open to the public.

## Release process overview

The release process follows [this set of templates](https://gitlab.com/gitlab-org/release-tools/tree/master/templates). Additional supporting docs about the release process can be found [here](https://gitlab.com/gitlab-org/release/docs/).

* First Deployable RC
    * Automated tests are run against Staging and Canary
    * An Issue is created listing the QA tasks for this release candidate
        * The items from the Release Kickoff Doc are copied to the Issue and associated with the appropriate PM
        * The PM performs testing on staging and lists any issues found
        * Other changes related to bugs and improvements are gathered from a Git comparison of this release from the previous one
        * The Engineering teams confirms that these other changes come with tests or have been tested on staging
    * After 24 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production
    * Example: [10.6 RC1 QA Task](https://gitlab.com/gitlab-org/release/tasks/issues/116)
* Subsequent RCs
    * Automated tests are run against Staging and Canary
    * An issue is created listing the QA tasks for this release candidate
        * The changes that had been merged in since the previous release are captured in a QA task Issue, associated with the appropriate engineer
        * The engineer performs testing on staging, or confirms that the change came with automated tests that are passing
    * After 12 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production
    * Example: [10.6 RC6 QA Task](https://gitlab.com/gitlab-org/release/tasks/issues/135)
* Final Release
    * Automated tests are run against Staging and Canary as a sanity check
    * Since no changes should have been included between the last RC and the release-day build, no additional testing or review should be required.
    * The final release candidate is deployed to production

## GitLab QA and Test Automation

The GitLab test automation framework is distributed across two projects:

* [GitLab QA], the test orchestration tool
* The scenarios and spec files within the GitLab main codebase

### Architecture overview

See the [GitLab QA Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs)
and [current architecture overview](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/architecture.md).

### Installation and execution

* Install and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
* Install and run [GitLab QA] to kick off test execution.
  * The spec files (test cases) can be found in the [GitLab CE/EE codebase](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/qa)

## Recruiting

* Information regarding the technical quizzes and assignments that are sent to candidates can be found [here](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires/blob/master/automation-engineer.md) (GITLAB ONLY)
* The steps of the hiring process can be found [here](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/engineering.md) (GITLAB ONLY)

## Common Links

* [On-boarding](onboarding)
* [Project Management](project-management)
* [Test Engineering](test-engineering)
* [Issue Triage](issue-triage/)
* [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce);
  please use confidential issues for topics that should only be visible to team members at GitLab.
* Chat channels; please use chat channels for questions that don't seem appropriate to use the issue tracker for.
  * [#quality](https://gitlab.slack.com/messages/quality): for general conversation about GitLab Quality
  * [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  * [#mr-coaching](https://gitlab.slack.com/archives/mr-coaching): for general
    conversation about Merge Request coaching.
  * [#opensource](https://gitlab.slack.com/archives/opensource): for general
    conversation about Open Source.

## Other related pages

* [Issue Triage Policies](/handbook/engineering/issue-triage)
* [Performance of GitLab](/handbook/engineering/performance)
* [Monitoring of GitLab.com](/handbook/engineering/monitoring)
* [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
