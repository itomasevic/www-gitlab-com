---
layout: markdown_page
title: "Blog response workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

## Workflow

1. Go through the tickets per-post
1. See if all comments have received responses
1. Respond if any comments need responses
1. Then mark all the relevant tickets as Solved.

You should also monitor the `#docs-comments` and `#mentions-of-gitlab` channels and mark every post with a `:white_check_mark:` to show it's been reviewed and handled.

## Best practices

## Automation

All the comments from our blog are handled by Disqus. There's an integration with ZenDesk in place which pipes posts to ZenDesk as tickets.
