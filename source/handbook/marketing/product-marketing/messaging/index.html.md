---
layout: markdown_page
title: "Product Marketing Messaging"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are the key customer problems?

Teams across the software delivery lifecycle function struggle with:

* Usage of different tools preferred by each team or individual
* Multiple integrations across the tools to make the lifecycle work
* Multiple configuration challenges
* Different work processes across teams
* Lack of common metrics across teams to measure improvements
* Sequential flow of processes and handoffs that are slow, error prone, and brittle

And they have processes which block reducing time to value, for example:

* Security reviews that are blocking until approved
* Infrastructure that has to be provisioned
* Fixed release windows
* Production teams need to manually approve releases
* SOX compliance sign offs need to happen
* Long testing cycles
* Separate build, QA, security, governance, and operations teams working in silos
* Hard to diagnose and time consuming to fix production failures

## GitLab value proposition - How does GitLab help solve the customer problems?

### Single sentence

GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 200% faster and radically improving the speed of business.

### Short message (~50 words)

GitLab is the first single application for the entire DevOps lifecycle. Only GitLab enables Concurrent DevOps, unlocking organizations from the constraints of the toolchain. GitLab provides unmatched visibility, higher levels of efficiency, and comprehensive governance. This makes the software lifecycle 200% times faster, radically improving the speed of business.

### Medium message (~250 words)

GitLab is the first single application for the entire DevOps lifecycle. Only GitLab enables Concurrent DevOps, unlocking organizations from the constraints of today’s toolchain. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect. This makes the software lifecycle 200% faster, radically improving the speed of business.

GitLab and Concurrent DevOps collapses cycle times by driving higher efficiency across all stages of the software development lifecycle. For the first time, Product, Development, QA, Security, and Operations teams can work concurrently in a single application. There’s no need to integrate and synchronize tools, or waste time waiting for handoffs. Everyone contributes to a single conversation, instead of managing multiple threads across disparate tools. And only GitLab gives teams complete visibility across the lifecycle with a single, trusted source of data to simplify troubleshooting and drive accountability. All activity is governed by consistent controls, making security and compliance first-class citizens instead of an afterthought.

Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.

### Long message (~450 Words)

GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 200% faster and radically improving the speed of business. Only GitLab provides a single application that unlocks organizations from the compromises and constraints of today’s DevOps Toolchain, significantly improving visibility, efficiency and governance. Now, fast paced teams no longer have to integrate or synchronize multiple DevOps tools and are able to go faster by working seamlessly across the complete lifecycle.

GitLab delivers complete real-time visibility of all projects and relevant activities across the entire DevOps lifecycle. For the first time, teams can see everything that matters. Changes, status, cycle times, security and operational health are instantly available from a trusted single source of data. Information is shown where it matters most, e.g. production impact is show together with the code changes that caused it. And developers see all relevant security and ops information for any change. With GitLab, there is never any need to wait on synchronizing your monitoring app to version control or copying information from tool to tool. GitLab frees teams to manage projects, not tools. These powerful capabilities eliminate guesswork, help teams drive accountability and gives everyone the data-driven confidence to act with new certainty.  With Gitlab, DevOps teams get better every day by having the visibility to see progress and operate with a deeper understanding of cycle times across projects and activities.

GitLab drives radically faster cycle times by helping DevOps teams achieve higher levels of efficiency across all stages of the lifecycle. Concurrent DevOps makes it possible for Product, Development, QA, Security, and Operations teams to work at the same time, instead of waiting for handoffs. Teams can work concurrently and review changes together before pushing to production. And everyone can contribute to a single conversation across every stage. Only GitLab eliminates the need to manually configure and integrate multiple tools for each project. Teams can start immediately and work concurrently to radically compress time across every stage of the DevOps lifecycle.

Only GitLab delivers DevOps teams powerful new governance capabilities embedded across the expanded lifecycle to automate security, code quality and vulnerability management. With GitLab, tighter governance and control never slow down DevOps speed.

GitLab leads the next advancement of DevOps. Built on Open Source, GitLab  delivers new innovations and features on the same day of every month by leveraging contributions from a passionate, global community of thousands of developers and millions of users. Over 100,000 of the world’s most demanding organizations trust GitLab to realize the transformative power of Concurrent DevOps to achieve a 3x faster lifecycle.

### Company overview text
The [company overview](https://about.gitlab.com/company/#about-us) can be found here.

### Press release boiler plate
The [press release boiler plate](https://about.gitlab.com/press/press-kit/#boiler-plate) can be found here.

### Standard Email introduction text

GitLab makes it easier for companies to achieve software excellence so that they can unlock great software for their customers by reducing the cycle time between having an idea and seeing it in production. GitLab does this by having an [integrated product](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) for the entire software development lifecycle. It contains not only issue management, version control and code review but also continuous integration, continuous delivery, and monitoring. More than 100,000 global organizations and millions of users use GitLab - in fact, 2/3 of organizations that self-host git use GitLab.




## Key messaging guidelines

### GitLab is not a platform

GitLab is not a platform, it's a single application.

A platform implies a surface on which smaller applications are run or integrated [see the dictionary.com definition](http://www.dictionary.com/browse/software-platform).
Rather, GitLab is a single application that offers the typical features of multiple applications through a single interface.

This is a key differentiation of GitLab in comparison with our competitors, with [many benefits](/handbook/product/single-application).

### Pricing tier messaging

Go to the [Pricing tier page](/handbook/marketing/product-marketing/tiers/) to see [Tier Messaging dos and don'ts](/handbook/marketing/product-marketing/tiers/#messaging-dos-and-donts)

## GitLab positioning FAQs

Go to the [GitLab positioning FAQ page](/handbook/positioning-faq/).
