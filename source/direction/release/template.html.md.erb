---
layout: markdown_page
title: "Product Vision - Release"
---

- TOC
{:toc}

## Overview

This is the product vision for Release. The Release stage of the DevOps pipeline
is focused on two main areas: [Continuous Delivery and Release Automation](https://go.forrester.com/blogs/continuous-delivery-and-release-automation-the-missing-link-for-business-transformation/) and [Application Release Automation/Orchestration](https://en.wikipedia.org/wiki/Application-release_automation).
This covers automation, repeatability, and risk-reduction features for releases, such as CD and
feature flags, as well as the pages feature. We want to make delivering software
reliable, repeatable, and successful. Please read through to understand where
we're headed and feel free to jump right into the discussion by commenting on
one of the epics or issues, or by reaching out directly to our product manager
leading this area: Jason Lenny ([E-Mail](mailto:jason@gitlab.com) | [Twitter](https://twitter.com/j4lenn))

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

## Release Automation & Orchestration Landscape

It's an exciting time in the world of Release Automation and Orchestration. Technologies like
Kubernetes have created a huge splash and are driving innovation forward;
serverless, microservices, and cloud native in general represent important
evolutions as well. Monitoring technology also continues to advance, making the
promise of technologies like automated rollbacks based on impact a reality.

At the same time, there are more tools and best-practice solutions to help solve
your delivery challenges. The bar is being raised, and clear best-
practice solutions are becoming more clear. It's important in this landscape that we
continue to refine existing features in our portfolio to ensure they are actually
solving real world problems in the best way possible.

## Upcoming Focus & Category Epics

Our Release stage strategy consists of developing several different categories 
which we've identified as critical for successful release orchestration. Overall, 
our vision is to continue to up-level features in the CD area, where we 
already have a solid foundation for delivery. Pages and Review Apps will continue 
to receive attention and improvements, but a big focus will be on improving the 
capabilities of our new (from Q4 2018) Feature Flags category. Finally, growing 
Release Orchestration capability is a priority as we look to help large enterprises 
achieve success with their DevOps transformations.

You can read more detail on each of these areas at the following epics. If you have
thoughts or questions, feel free to jump into the conversation there.

- [Release Orchestration](https://gitlab.com/groups/gitlab-org/-/epics/491): Release Orchestration is the ability to coordinate complex releases, particularly across projects, in an efficient way that leverages as-code behaviors as much as possible, but also recognizes that there are manual steps and coordination points involving human decisions throughout software delivery in the enterprise. More specifically, this is managing the kinds of enterprise releases for which you'd have a Release Manager in play, rather than having individual teams continually deploying independent code to production.
- [Feature Flags](https://gitlab.com/groups/gitlab-org/-/epics/492): Feature flags unlock faster, more agile delivery workflows by providing native support for feature flags directly into your development and delivery process.
- [Pages](https://gitlab.com/groups/gitlab-org/-/epics/493): Pages allows you to automatically document and deliver your content through your delivery pipelines with built-in content hosting support.
- [Continuous Delivery](https://gitlab.com/groups/gitlab-org/-/epics/494): Many teams are reporting that development velocity is stuck; they've reached a plateau in moving to more and more releases per month, and need help on how to improve. At the moment, 40% of software development team's top priorities relate to speed/automation. Our overriding vision for Continuous Delivery is to help these teams renew their ability to accelerate.
- [Review Apps](https://gitlab.com/groups/gitlab-org/-/epics/495): Review Apps let you build reviews right into your software development workflow by automatically provisioning test environments for your code, integrated right into your merge requests.
- [Mobile Publishing](https://gitlab.com/groups/gitlab-org/-/epics/497): More and more companies have at least one mobile app in development, and some have several for different areas of the business. Some companies, such as mobile game publishers, are doing incredibly complicated and fast paced delivery of hundreds of mobile apps every day. One of the most interesting aspects of working on mobile is managing the process of publishing and certification in mobile marketplaces (Apple Store, Android Marketplace, etc.) Also, distribution of trial/demo builds pre-release is a unique problem space. There is certainly more we can do here to make all of this easier.
- [Binary Authorization](https://gitlab.com/groups/gitlab-org/-/epics/341): Binary Authorization is a deploy-time security control that ensures only trusted container images are deployed on Kubernetes Engine.

## North Star Principles

In order to accelerate CD in this new world, there are a few particular ideas we
are keeping close as north stars to guide us forward:

### Platform Ubiquity

Our platform must stay current with evolving trends in platform architecture.
Cloud-Native, Microservices, Kubernetes, and Serverless will continue to lead the way here,
and our CI/CD solutions must address the unique needs of these approaches by
offering solutions that facilitate the technological and cultural
transformations these teams are going through. These technologies represent a
wave driving DevOps forward, and we want to be on the crest of that wave helping
companies to deliver using GitLab.

### Cultural Transformation

As Peter Drucker said, "if you can't measure it - you can't improve it." Using
the data in our CI/CD platform to help teams get the most out of their delivery
pipeline gives us a unique advantage in offering DevOps insights to our users.
Where competitors must integrate with a variety of other tools, attempting to
normalize and understand data structures that can change at any time, we build a
single application solution where process, behavioral, and other insights can
flow seamlessly throughout, facilitating organizational transformation. Value
stream mapping, wait time, retries, failure rate, batch size, job duration,
quality, resource usage, throughput; these (and more) are all great metrics we
already have in the system and can increase visibility to.

### More Complete (Minimally Lovable) Features to Solve Complex Problems

V1 feature iterations are how we build software, but at the same time we need to
continue to curate those features that have proven their value into complete,
lovable features that exceed our users expectations. We will achieve this by
growing individual features, solving scalability challenges that larger
customers see, and providing intelligent connections between individual
features. Doing this lets us solve deeper, more subtle problem sets and - by
being focused on real problems our users face - we'll leave the competition
behind. This also includes solid, natural-to-use UX and complete APIs.

## Roadmap

It's important to mention that what you see below is our vision on the product at
this time. All of this can change any moment and should not be taken as a hard
commitment, though we do try to keep things generally stable and not change
things all the time.

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "release" }) %>
